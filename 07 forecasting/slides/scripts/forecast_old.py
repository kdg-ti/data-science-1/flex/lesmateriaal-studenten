import numpy as np


# %% only forecasts for past values
def predict(y: np.array, f, *argv):
    return np.array([f(y[:i], *argv) for i in range(y.size)])


# %% only forecasts for past values using a generator
def predict_gen(y: np.array, f, *argv):
    i = 0
    while True:
        yield f(y[:i], *argv)
        i += 1


# %% only predicts nf-steps forward with NaNs for pasts
def predict_forward(y: np.array, nf, f, *argv):
    prediction = predict_recusively(y, nf, f, *argv)
    prediction[:y.size] = np.nan
    return prediction


# %% recursively predict forward from start to nf-steps beyond end
def predict_recusively(y: np.array, nf, f, *argv):
    if nf < 1:
        return y
    return predict_recusively(np.append(y, f(y, *argv)), nf - 1, f, *argv)
