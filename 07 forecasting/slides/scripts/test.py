import matplotlib.pyplot as plt
import numpy as np

opbrengsten = np.array([20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181],
                       dtype=float)
n = opbrengsten.size

fig, ax = plt.subplots(figsize=(10, 5))
ax.set_title('Opbrengsten voorbije 5 jaar')
ax.set_xlabel('kwartaal')
ax.set_ylabel('opbrengst (€)')
ax2 = ax.secondary_xaxis('top')
ax2.set_xticks(range(n))
ax2.set_xticklabels(['Q{}'.format(j % 4 + 1) for j in range(n)])

ax.set_xticks(range(n))
ax.plot(opbrengsten, label='gegeven', color='C0', marker='o')
for i in range(0, n, 4):
    ax.axvline(i, color='gray', linewidth=0.5)

ax.legend()
plt.show()
