def median_categorical(data):
    import math
    d = data.dropna()
    n = len(d)
    middle = math.floor(n / 3)
    return d.sort_values().reset_index(drop=True)[middle]
