import numpy as np
import pandas as pd
from datetime import timedelta


def preprocess(enquete_df: pd.DataFrame):
    """ Maakt een apart dataframe met daarin de vraagnummers en de bijhorende vragen """

    # maak een vragen dataframe aan
    vragen_df = pd.DataFrame(enquete_df.columns.to_numpy(), columns=['vraag'])

    # extraheer de vraagnummers en de vragen
    vragen_df.index = vragen_df.vraag.replace('(\\d{1,2}[a-z]?).*', 'v\\1', regex=True)
    vragen_df = vragen_df \
        .replace('(\\d{1,2}[a-z]?)(.*)', '\\2', regex=True) \
        .replace('[.]', ' ', regex=True) \
        .replace('(^  )|( $)', ' ', regex=True)

    # vervang de originele kolomnamen door de nieuwe kortere namen.
    enquete_df.columns = vragen_df.index.rename('id')

    process_datetimes(enquete_df)
    process_categorical(enquete_df)
    process_numerical(enquete_df)
    fruit_voorkeuren, mascotte_voorkeuren = process_preferences(enquete_df)
    vervoer_df = process_transport(enquete_df)
    vragen_df['datatype'] = enquete_df.dtypes

    return enquete_df, vragen_df, fruit_voorkeuren, mascotte_voorkeuren, vervoer_df


def process_datetimes(enquete_df: pd.DataFrame):
    # timestamp parsen als datetime object
    enquete_df.Timestamp = enquete_df.Timestamp.apply(pd.to_datetime, format='%Y/%m/%d %I:%M:%S %p %Z')

    # v14: aantal uren gewerkt voor school parsen als timedelta object
    enquete_df.v14 = enquete_df.v14.apply(
        lambda row: timedelta(hours=int(row.split(':')[0]), minutes=int(row.split(':')[1])))

    # v17: opening horeca parsen als datetime (vb. 2021-06-30)
    enquete_df.v17 = enquete_df.v17.apply(pd.to_datetime, format='%Y-%m-%d')


def process_categorical(enquete_df: pd.DataFrame):
    # omzetten letters voornamen naar uppercase en NaN
    enquete_df.v1b = enquete_df.v1b.str.upper()  # alles omzetten naar uppercase
    enquete_df.loc[~enquete_df.v1b.str.isalpha(), ['v1b']] = pd.NA  # niet alfanumerieke inhoud omzetten naar NA-waarde

    # omzetten naar nominale meetschaal
    for column in ['v1b', 'v2', 'v3', 'v4', 'v7a', 'v7b', 'v10', 'v11', 'v21', 'v22', 'v25', 'v26']:
        enquete_df[column] = enquete_df[column].astype(
            pd.CategoricalDtype(categories=enquete_df[column].dropna().unique()))

    # omzetten naar ordinale meetschaal
    enquete_df.v13 = enquete_df.v13.astype(
        pd.CategoricalDtype(categories=['Niet', 'Weinig', 'Matig', 'Groot', 'Zeer Groot', 'Extreem'], ordered=True))


def process_numerical(enquete_df: pd.DataFrame):
    # v1a : scores op GapMinder omzetten naar floats
    v1a = enquete_df.v1a.copy()  # kopie maken om SettingWithCopyWarning te vermijden

    # v1a.name = 'v1a_bis'                  # nodig indien je wil joinen met origineel
    # breuken 1/x en 1 op x omzetten naar percentages
    breuken1 = ['{}/{}'.format(i, '\d\d') for i in np.arange(0, 14)]
    breuken2 = ['{}\s?op\s?{}'.format(i, '\d\d') for i in np.arange(0, 14)]
    percentages = ['{:.0f}%'.format(i) for i in np.round(np.arange(0, 14) / np.full(14, 13) * 100, 0)]

    # vervang breuknotaties door percentagenotaties
    v1a = v1a.replace(breuken1, percentages, regex=True).replace(breuken2, percentages, regex=True)

    # zet alle percentages om naar floats of NaN
    enquete_df.v1a = v1a.replace('(\d?\d)%.*', '\\1', regex=True).apply(pd.to_numeric, errors='coerce')

    # v18 zakgeld omzetten naar floats
    enquete_df.v18 = enquete_df.v18.str.extract('(\d+.?\d?)').apply(pd.to_numeric, errors='coerce')
    enquete_df.v19 = enquete_df.v19.str.extract('(\d+.?\d?)').apply(pd.to_numeric, errors='coerce')

    # v27 studiepunten omzetten naar gemiddelden
    enquete_df.v27 = enquete_df.v27.replace('(\D*)(\d+)(-\d+)?.*', '\\2\\3', regex=True).str.split('-').apply(
        pd.to_numeric).apply(np.mean)


def process_preferences(enquete_df: pd.DataFrame):
    fruit_voorkeuren = pd.DataFrame(
        zip(*enquete_df.v20
            .apply(splits_keuzes)
            .apply(splits_voorkeuren)
            .apply(sorteer_voorkeuren)), index=range(1, 11)).transpose()

    mascotte_voorkeuren = pd.DataFrame(
        zip(*enquete_df.v24
            .apply(splits_keuzes)
            .apply(splits_voorkeuren)
            .apply(sorteer_voorkeuren)), index=range(1, 11)).transpose()

    return fruit_voorkeuren, mascotte_voorkeuren


def process_transport(enquete_df: pd.DataFrame):
    return pd.DataFrame(enquete_df.v16.str.split(";").apply(extraheer_vervoermiddelen))


# hulpfuncties voor voorkeuren
def splits_keuzes(row):
    return str.split(row, ',')


def splits_voorkeuren(row):
    return np.array([str.split(el, '=') for el in row])


def sorteer_voorkeuren(row):
    try:
        rangnummers = row[:, 0].astype(int)  # eerste kolom bevat de rangnummers
        return row[rangnummers.argsort()][:, 1]  # tweede kolom bevat de fruitsoorten
    except Exception:  # bv. indien geen cijfers gebruikt heeft
        return np.repeat(np.nan, 10)


def extraheer_vervoermiddelen(row):
    vervoer_middelen = np.full(6, 0)

    if 'Te voet' in row:
        vervoer_middelen[0] = 1
    if 'Met de fiets' in row:
        vervoer_middelen[1] = 1
    if 'Met de bus' in row:
        vervoer_middelen[2] = 1
    if 'Met de tram' in row:
        vervoer_middelen[3] = 1
    if 'Met de trein' in row:
        vervoer_middelen[4] = 1
    if 'Met de auto' in row:
        vervoer_middelen[5] = 1

    return pd.Series(vervoer_middelen,
                     index=['Te voet', 'Met de fiets', 'Met de bus', 'Met de tram', 'Met de trein', 'Met de auto'])
