# Oefenmateriaal voor Data Science 1

Dit is de repository voor het oefenmateriaal voor het vak Data Science 1.  

* Onder **oefeningen** vind je notebooks met de opgaven, images en datasets en soms ook oefenboekjes.
* Onder de **slides** folder vind je notebooks die aansluiten bij de slides. Je kan deze gebruiken om de leerstof in te
oefenen. Soms zijn daar ook de **datasets** bij die aan bod komen in de slides.

