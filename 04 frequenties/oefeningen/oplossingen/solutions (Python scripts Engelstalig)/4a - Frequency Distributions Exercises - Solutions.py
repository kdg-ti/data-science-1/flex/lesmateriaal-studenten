
import os
os.chdir('F:/Python_workspace/DataScience/Sources')
import pandas as pd
from IPython.display import display
import matplotlib.pyplot as plt


"""Question 1"""
#Answer: Bar graph

"""Question 2"""
#2 a Pie chart
# import matplotlib.pyplot as plt

freq = [17,5,3,1,2,2]
fruit = ["apple", "banana", "grapes", "pear", "melon", "kiwi"]
plt.figure()
plt.pie(freq, labels=fruit)
plt.title("Fruit Consumption")
plt.show()

#2 b Bar chart
plt.figure()
plt.bar(fruit, freq)
plt.title("Fruit Consumption")
plt.xlabel("Type of Fruit")
plt.ylabel("Number")
plt.show()

"""Question 3"""
#3 a
# import pandas as pd
x = [4,5,6,7,8,9,10]
freq = [2,3,5,6,4,2,3]
df = pd.DataFrame({'freq':freq}, columns = ['freq'],index=x)
cum_perc = df.cumsum()/df.sum()*100
cum_perc.columns = ['cum perc']
display(cum_perc)
print("What cumulative percentage is associated with score 6")
print(cum_perc.loc[6]['cum perc'],"%")
print("What percentile score is associated with score 8 ")
print(int(cum_perc.loc[8]['cum perc']))

"""Question 4"""
category=["A", "B", "C", "D"]
income=["1000 tot 2000", "2000 tot 3000", "3000 tot 4000", "4000 tot 5000"]
frequency=[30,40,30,45]
df = pd.DataFrame({'income':income,'frequency':frequency}, columns = ['income','frequency'],index=category)
df["rel. frequency"]=(df["frequency"]/df["frequency"].sum()*100).round(1)
df["cum. percentage"]=(df["frequency"].cumsum()/df["frequency"].sum()*100).round(1)
df["percentielscore"]=df["cum. percentage"]
df["rel. frequency"]=df["rel. frequency"].astype(str)+' %'
df["cum. percentage"]=df["cum. percentage"].astype(str)+' %'
display(df)
print("What is the cumulative percentage of income category C?")
print(df.loc['C']['cum. percentage'])

"""Question 5"""
# Answer: (c)

"""Question 6"""
# Answer: 2

"""Question 7"""
# Answer: True

"""Question 8"""
# import numpy as np
score = range(1,11,1) #add values 1 and 10
frequency=[0,1,3,2,3,5,6,3,2,0] #add 0 as frequency for the values 1 and 10
df = pd.DataFrame({'frequency':frequency}, columns = ['frequency'],index=score)
plt.figure()
df.plot.hist() # This will not give you the right histogram!!!
plt.show()

#use of bar which looks like a histogram:
score = range(1,11,1) #add values 1 and 10
frequency=[0,1,3,2,3,5,6,3,2,0] #add 0 as frequency for the values 1 and 10
plt.figure()
ax = plt.axes()
ax.set_xticks(score)
plt.bar(score, frequency, 1.0, color='y')
plt.show()

print("Which cumulative percentage is associated with score 6")
cum_perc = df.cumsum()/df.sum()*100
cum_perc.columns = ['cum perc']
display(cum_perc)
print(cum_perc.loc[6]['cum perc'],"%")

"""Question 9"""
#import numpy as np
#import pandas as pd
x=range(4,12,1)
freq=[5,7,9,11,12,10,8,6]
df = pd.DataFrame({'frequency':freq}, columns = ['frequency'],index=x)
df["rel. frequency"]=(df["frequency"]/df["frequency"].sum()*100).round(1)
df["cum. percentage"]=(df["frequency"].cumsum()/df["frequency"].sum()*100).round(1)
df["percentile score"]=df["cum. percentage"]
df["rel. frequency"]=df["rel. frequency"].astype(str)+' %'
df["cum. percentage"]=df["cum. percentage"].astype(str)+' %'
display(df)

print("What is the percentile score associated with a score of 9?")
print(df.loc[9]['percentile score'])
print("What is the percentile score associated with a score of 7.5?")
# Same as the percentile score associated with a score of 7
print(df.loc[7]['percentile score'])
print("What is the percentile score associated with a percentile score of 35")
#Answer: 7

"""Question 10"""
# import pandas as pd
# (a) Load data
sat_surv = pd.read_csv('satisfaction.txt',header=None,names=['values'])
display(sat_surv)
sat_surv.info()

# (b) frequency table
table = all_freq(sat_surv['values'])

# (c)
plt.figure()
table['rel freq'].plot.bar()
plt.show()

# (d)
# Answer: 6.9%