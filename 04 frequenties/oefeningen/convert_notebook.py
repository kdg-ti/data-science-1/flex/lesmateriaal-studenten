import nbformat
import os

# nb = nbformat.read('oplossingen/Oefenboek op Numpy - oplossingen.ipynb', as_version=4)
#
# for cell in nb.cells.copy():
#     if 'code' in cell.cell_type:
#         nb.cells.remove(cell)
#
# nbformat.write(nb, 'opgaven/Oefenboek op Numpy - oplossingen.ipynb')
from nbformat.v4 import new_code_cell

for filename in os.listdir('oplossingen'):
    if filename.endswith('.ipynb'):
        nb = nbformat.read('oplossingen/'+filename, as_version=4)
        for i, cell in enumerate(nb.cells.copy()):
            if 'code' in cell.cell_type:
                if '# keep' not in cell.source:
                    nb.cells.remove(cell)
                    newcell = new_code_cell(source='# hier komt jouw oplossing')
                    nb.cells.insert(i, newcell)
        nbformat.write(nb, 'opgaven/'+filename.replace(' - oplossingen', ''))
